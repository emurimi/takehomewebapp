A simple “Bank Account” web service using REST API design principles using Java/Spring/Tomcat framework.
========================================================================================================
Database
#############################3
Configuring the Database

The database used is MySql version 5.6.24 - MySQL Community Server (GPL)
Import the database file under DB directory (bankaccount.sql)
Update springrest-servlet.xml located under takeHomeWebapp\src\main\webapp\WEB-INF to fit your database configuration, a sample of the database section is shown below.

<beans:bean id="dataSource" class="org.apache.commons.dbcp.BasicDataSource"
		destroy-method="close">
		<beans:property name="driverClassName" value="com.mysql.jdbc.Driver" />
		<beans:property name="url"
			value="jdbc:mysql://localhost:3306/bankaccount" />
		<beans:property name="username" value="root" />
		<beans:property name="password" value="" />
	</beans:bean>
	
#####################################
Running the takeHomeWebapp
###########################
This example runs in Apache Tomcat.
Shutdown Tomcat if running using the shutdown bat file located in bin directory, I am using apache-tomcat-7.0.82 for this project
package the .war file and copy it to the webapp folder of Tomcat.
Start Tomcat using the startup bat file in the bin folder

####################################
Sample API calls
####################################

************************************
Balance 
http://localhost:8080/takeHomeWebapp/balance
METHOD: GET

Sample response

[
    {
        "balance": 5500
    }
]

************************************
View all Deposits and Withdrawals
http://localhost:8080/takeHomeWebapp/transactions
METHOD: GET

sample response: [
    {
        "id": 6,
        "accountNumber": "4242424242424242",
        "transactionAmount": 4000,
        "transactionType": "D",
        "transactionDate": 1511470839000
    },
    {
        "id": 9,
        "accountNumber": "4242424242424242",
        "transactionAmount": 500,
        "transactionType": "W",
        "transactionDate": 1511516346000
    },
    {
        "id": 10,
        "accountNumber": "4242424242424242",
        "transactionAmount": 1000,
        "transactionType": "D",
        "transactionDate": 1511519717000
    },
    {
        "id": 11,
        "accountNumber": "4242424242424242",
        "transactionAmount": 1000,
        "transactionType": "D",
        "transactionDate": 1511533069000
    },
    {
        "id": 12,
        "accountNumber": "4242424242424242",
        "transactionAmount": 4000,
        "transactionType": "D",
        "transactionDate": 1511533092000
    },
    {
        "id": 13,
        "accountNumber": "4242424242424242",
        "transactionAmount": 4000,
        "transactionType": "W",
        "transactionDate": 1511537404000
    }
]

***************************************
Deposit
http://localhost:8080/takeHomeWebapp/deposit
METHOD:POST
Sample Request:
{"accountNumber":"4242424242424242","transactionType":"D","transactionAmount":6000.0}

Sample response:

[
    {
        "id": 3,
        "code": "210",
        "message": "Failed: You have reached the maximum number of transactions allowed per day"
    }
]

****************************************
Withdrawals
http://localhost:8080/takeHomeWebapp/withdraw
METHOD:POST
Sample Request:
METHOD:POST
Sample Request:
{"accountNumber":"4242424242424242","transactionType":"W","transactionAmount":6000.0}


Sample response:

[
    {
        "id": 10,
        "code": "220",
        "message": "Failed: You have insufficient funds to complete this Withdrawal"
    }
]

