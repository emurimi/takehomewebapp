-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 24, 2017 at 04:47 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bankaccount`
--
CREATE DATABASE IF NOT EXISTS `bankaccount` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `bankaccount`;

-- --------------------------------------------------------

--
-- Table structure for table `tbaccounts`
--

DROP TABLE IF EXISTS `tbaccounts`;
CREATE TABLE IF NOT EXISTS `tbaccounts` (
  `id` int(11) NOT NULL,
  `account_number` varchar(16) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbaccounts`
--

INSERT INTO `tbaccounts` (`id`, `account_number`) VALUES
(1, '4242424242424242');

-- --------------------------------------------------------

--
-- Table structure for table `tbaccounttransactions`
--

DROP TABLE IF EXISTS `tbaccounttransactions`;
CREATE TABLE IF NOT EXISTS `tbaccounttransactions` (
  `id` int(4) NOT NULL,
  `account_number` varchar(16) DEFAULT NULL,
  `transaction_type` varchar(1) DEFAULT NULL,
  `transaction_amount` float DEFAULT NULL,
  `transaction_date` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbaccounttransactions`
--

INSERT INTO `tbaccounttransactions` (`id`, `account_number`, `transaction_type`, `transaction_amount`, `transaction_date`) VALUES
(6, '4242424242424242', 'D', 4000, '2017-11-24 00:00:39'),
(9, '4242424242424242', 'W', 500, '2017-11-24 12:39:06'),
(10, '4242424242424242', 'D', 1000, '2017-11-24 13:35:17'),
(11, '4242424242424242', 'D', 1000, '2017-11-24 17:17:49'),
(12, '4242424242424242', 'D', 4000, '2017-11-24 17:18:12'),
(13, '4242424242424242', 'W', 4000, '2017-11-24 18:30:04');

-- --------------------------------------------------------

--
-- Table structure for table `tbbalance`
--

DROP TABLE IF EXISTS `tbbalance`;
CREATE TABLE IF NOT EXISTS `tbbalance` (
  `id` int(2) NOT NULL,
  `balance` float NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbbalance`
--

INSERT INTO `tbbalance` (`id`, `balance`) VALUES
(1, 2000);

-- --------------------------------------------------------

--
-- Table structure for table `tb_errormessages`
--

DROP TABLE IF EXISTS `tb_errormessages`;
CREATE TABLE IF NOT EXISTS `tb_errormessages` (
  `id` int(2) NOT NULL,
  `code` varchar(3) DEFAULT NULL,
  `message` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_errormessages`
--

INSERT INTO `tb_errormessages` (`id`, `code`, `message`) VALUES
(1, '200', 'Your request has been processed with success'),
(2, '205', 'Exceeded Maximum Deposit Per\nTransaction'),
(3, '210', 'Failed: You have reached the maximum number of transactions allowed per day'),
(5, '212', 'Failed: You have reached the daily deposit limit amount.'),
(6, '213', 'Exceeded Maximum Withdrawal Per\nTransaction'),
(7, '213', 'Failed: You have reached the daily Withdrawl limit amount of 40,000'),
(10, '220', 'Failed: You have insufficient funds to complete this Withdrawal');

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_balance`
--
DROP VIEW IF EXISTS `vw_balance`;
CREATE TABLE IF NOT EXISTS `vw_balance` (
`id` int(1)
,`balance` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_depositscount`
--
DROP VIEW IF EXISTS `vw_depositscount`;
CREATE TABLE IF NOT EXISTS `vw_depositscount` (
`id` int(1)
,`deposits_count` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_sumdeposits`
--
DROP VIEW IF EXISTS `vw_sumdeposits`;
CREATE TABLE IF NOT EXISTS `vw_sumdeposits` (
`sumdeposits` double
,`account_number` varchar(16)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_sumdepositstoday`
--
DROP VIEW IF EXISTS `vw_sumdepositstoday`;
CREATE TABLE IF NOT EXISTS `vw_sumdepositstoday` (
`id` int(1)
,`total_deposits` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_sumwithdrawals`
--
DROP VIEW IF EXISTS `vw_sumwithdrawals`;
CREATE TABLE IF NOT EXISTS `vw_sumwithdrawals` (
`sumwithdrawals` double
,`account_number` varchar(16)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_sumwithdrawalstoday`
--
DROP VIEW IF EXISTS `vw_sumwithdrawalstoday`;
CREATE TABLE IF NOT EXISTS `vw_sumwithdrawalstoday` (
`id` int(1)
,`total_withdrawals` double
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_withdrawalscount`
--
DROP VIEW IF EXISTS `vw_withdrawalscount`;
CREATE TABLE IF NOT EXISTS `vw_withdrawalscount` (
`id` int(1)
,`withdrawals_count` bigint(21)
);

-- --------------------------------------------------------

--
-- Structure for view `vw_balance`
--
DROP TABLE IF EXISTS `vw_balance`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_balance` AS select 1 AS `id`,(coalesce((select sum(`tbaccounttransactions`.`transaction_amount`) from `tbaccounttransactions` where (`tbaccounttransactions`.`transaction_type` = 'D')),0) - coalesce((select sum(`tbaccounttransactions`.`transaction_amount`) from `tbaccounttransactions` where (`tbaccounttransactions`.`transaction_type` = 'W')),0)) AS `balance`;

-- --------------------------------------------------------

--
-- Structure for view `vw_depositscount`
--
DROP TABLE IF EXISTS `vw_depositscount`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_depositscount` AS select 1 AS `id`,count(`tbaccounttransactions`.`id`) AS `deposits_count` from `tbaccounttransactions` where ((`tbaccounttransactions`.`transaction_type` = 'D') and (cast(`tbaccounttransactions`.`transaction_date` as date) = curdate()));

-- --------------------------------------------------------

--
-- Structure for view `vw_sumdeposits`
--
DROP TABLE IF EXISTS `vw_sumdeposits`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_sumdeposits` AS select sum(`tbaccounttransactions`.`transaction_amount`) AS `sumdeposits`,`tbaccounttransactions`.`account_number` AS `account_number` from `tbaccounttransactions` where ((`tbaccounttransactions`.`transaction_type` = 'D') and (cast(`tbaccounttransactions`.`transaction_date` as date) = curdate()));

-- --------------------------------------------------------

--
-- Structure for view `vw_sumdepositstoday`
--
DROP TABLE IF EXISTS `vw_sumdepositstoday`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_sumdepositstoday` AS select 1 AS `id`,sum(`tbaccounttransactions`.`transaction_amount`) AS `total_deposits` from `tbaccounttransactions` where ((cast(`tbaccounttransactions`.`transaction_date` as date) = curdate()) and (`tbaccounttransactions`.`transaction_type` = 'D'));

-- --------------------------------------------------------

--
-- Structure for view `vw_sumwithdrawals`
--
DROP TABLE IF EXISTS `vw_sumwithdrawals`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_sumwithdrawals` AS select sum(`tbaccounttransactions`.`transaction_amount`) AS `sumwithdrawals`,`tbaccounttransactions`.`account_number` AS `account_number` from `tbaccounttransactions` where ((`tbaccounttransactions`.`transaction_type` = 'W') and (cast(`tbaccounttransactions`.`transaction_date` as date) = curdate()));

-- --------------------------------------------------------

--
-- Structure for view `vw_sumwithdrawalstoday`
--
DROP TABLE IF EXISTS `vw_sumwithdrawalstoday`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_sumwithdrawalstoday` AS select 1 AS `id`,sum(`tbaccounttransactions`.`transaction_amount`) AS `total_withdrawals` from `tbaccounttransactions` where ((cast(`tbaccounttransactions`.`transaction_date` as date) = curdate()) and (`tbaccounttransactions`.`transaction_type` = 'W'));

-- --------------------------------------------------------

--
-- Structure for view `vw_withdrawalscount`
--
DROP TABLE IF EXISTS `vw_withdrawalscount`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_withdrawalscount` AS select 1 AS `id`,count(`tbaccounttransactions`.`id`) AS `withdrawals_count` from `tbaccounttransactions` where ((`tbaccounttransactions`.`transaction_type` = 'W') and (cast(`tbaccounttransactions`.`transaction_date` as date) = curdate()));

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbaccounts`
--
ALTER TABLE `tbaccounts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbaccounttransactions`
--
ALTER TABLE `tbaccounttransactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbbalance`
--
ALTER TABLE `tbbalance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_errormessages`
--
ALTER TABLE `tb_errormessages`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbaccounts`
--
ALTER TABLE `tbaccounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbaccounttransactions`
--
ALTER TABLE `tbaccounttransactions`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tbbalance`
--
ALTER TABLE `tbbalance`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_errormessages`
--
ALTER TABLE `tb_errormessages`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
