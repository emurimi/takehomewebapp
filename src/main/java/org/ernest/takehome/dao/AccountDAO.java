package org.ernest.takehome.dao;
import java.util.List;

import org.ernest.takehome.model.Account;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AccountDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	
	public List<Account> getAllAccounts() {
		Session session = this.sessionFactory.getCurrentSession();		
		List<Account> accountList = session.createQuery("from Account").list();
		return accountList;
	}
	
	
	public Account getCountry(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Account country = (Account) session.load(Account.class, new Integer(id));
		return country;
	}

	public Account addCountry(Account country) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(country);
		return country;
	}

	public void updateCountry(Account country) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(country);
	}

	public void deleteCountry(int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Account p = (Account) session.load(Account.class, new Integer(id));
		if (null != p) {
			session.delete(p);
		}
	}	
}
