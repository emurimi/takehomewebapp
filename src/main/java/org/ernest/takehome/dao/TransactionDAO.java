package org.ernest.takehome.dao;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.ernest.takehome.model.Transaction;
import org.ernest.takehome.model.Balance;
import org.ernest.takehome.model.Errors;
import org.ernest.takehome.model.DepositsCount;
import org.ernest.takehome.model.WithdrawalsCount;
import org.ernest.takehome.model.SumDepositsToday;
import org.ernest.takehome.model.SumWithdrawalsToday;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class TransactionDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	
	public List<Transaction> getAllTransactions() {
		Session session = this.sessionFactory.getCurrentSession();		
		List<Transaction> transactionList = session.createQuery("from Transaction").list();
		return transactionList;
	}
	
	

//
	public Transaction addDeposit(Transaction deposit) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(deposit);
		return deposit;
	}
	
	
	public Transaction addWithdraw(Transaction withdraw) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(withdraw);
		return withdraw;
	}
	
	 
	
	public List<Transaction> getAccountBalance() {
		Session session = this.sessionFactory.getCurrentSession();		
		List<Transaction> transactionList = session.createQuery("from Balance").list();
		return transactionList;
	}
	
	public List<Balance> getAccountBalanceWithdraw() {
		Session session = this.sessionFactory.getCurrentSession();		
		List<Balance> transactionList = session.createQuery("from Balance").list();
		return transactionList;
	}
	
	
	public List<Transaction> getErrorMessage() {
		Session session = this.sessionFactory.getCurrentSession();		
		List<Transaction> errorMessage = session.createQuery("from Errors where code ='200'").list();
		return errorMessage;
	}
	
	public List<Transaction> getSuccessMessageWithdrawal() {
		Session session = this.sessionFactory.getCurrentSession();		
		List<Transaction> errorMessage = session.createQuery("from Errors where code ='200'").list();
		return errorMessage;
	}
	
	public List<Transaction> getInsufficientBalanceError() {
		Session session = this.sessionFactory.getCurrentSession();		
		List<Transaction> errorMessage = session.createQuery("from Errors where code ='220'").list();
		return errorMessage;
	}
	
	
	public List<Transaction> getDepositExceedErrorMessage() {
		Session session = this.sessionFactory.getCurrentSession();		
		List<Transaction> errorMessage = session.createQuery("from Errors where code ='205'").list();
		return errorMessage;
	}
	
	public List<Transaction> getWithdrawExceedErrorMessage() {
		Session session = this.sessionFactory.getCurrentSession();		
		List<Transaction> errorMessage = session.createQuery("from Errors where code ='212'").list();
		return errorMessage;
	}
	
	
	public List<Transaction> getDepositCountExceedErrorMessage() {
		Session session = this.sessionFactory.getCurrentSession();		
		List<Transaction> errorMessage = session.createQuery("from Errors where code ='210'").list();
		return errorMessage;
	}
	
	
	public List<Transaction> getDepositAmountExceedErrorMessage() {
		Session session = this.sessionFactory.getCurrentSession();		
		List<Transaction> errorMessage = session.createQuery("from Errors where code ='212'").list();
		return errorMessage;
	}
	
	public List<Transaction> getWithdrawalsAmountExceedErrorMessage() {
		Session session = this.sessionFactory.getCurrentSession();		
		List<Transaction> errorMessage = session.createQuery("from Errors where code ='213'").list();
		return errorMessage;
	}
	
	
	public List<DepositsCount> checkTrxLimit() {
		Session session = this.sessionFactory.getCurrentSession();		
		List<DepositsCount> depositscount = session.createQuery("from DepositsCount").list();
		return depositscount;
	}
	
	public List<WithdrawalsCount> checkTrxLimitWithdrawals() {
		Session session = this.sessionFactory.getCurrentSession();		
		List<WithdrawalsCount> withdrawalscount = session.createQuery("from WithdrawalsCount").list();
		return withdrawalscount;
	}
	
	public List<SumDepositsToday> checkSumDeposits() {
		Session session = this.sessionFactory.getCurrentSession();		
		List<SumDepositsToday> depositscount = session.createQuery("from SumDepositsToday").list();
		return depositscount;
	}
	
	public List<SumWithdrawalsToday> checkSumWithdrawals() {
		Session session = this.sessionFactory.getCurrentSession();		
		List<SumWithdrawalsToday> depositscount = session.createQuery("from SumWithdrawalsToday").list();
		return depositscount;
	}
	
	
	
//
//	public void updateCountry(Account country) {
//		Session session = this.sessionFactory.getCurrentSession();
//		session.update(country);
//	}
//
//	public void deleteCountry(int id) {
//		Session session = this.sessionFactory.getCurrentSession();
//		Account p = (Account) session.load(Account.class, new Integer(id));
//		if (null != p) {
//			session.delete(p);
//		}
//	}	
}
