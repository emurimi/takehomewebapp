package org.ernest.takehome.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

/*
 * This is our model class and it corresponds to tb_accounts table in database
 */
@Entity
@Table(name="tbaccounttransactions")
public class Transaction{
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	
	@Column(name="account_number")
	String account_number;	
	
	@Column(name="transaction_type")
	String transaction_type;
	
	@Column(name="transaction_amount")
	Double transaction_amount;

	
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "transaction_date")
	private Date transaction_date;
	
	
	public Transaction() {
		super();
	}
	public Transaction(int id, String account_number, String transaction_type, Double transaction_amount, Date transaction_date) {
		super();
		this.id = id;
		this.account_number = account_number;
		this.transaction_type = transaction_type;
		this.transaction_amount = transaction_amount;
		this.transaction_date = transaction_date;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAccountNumber() {
		return account_number;
	}
	public void setAccountNumber(String account_number) {
		this.account_number = account_number;
	}
	
	public String getTransactionType() {
		return transaction_type;
	}
	public void setTransactionType(String transaction_type) {
		this.transaction_type = transaction_type;
	}
	
	
	public Double getTransactionAmount() {
		return transaction_amount;
	}
	public void setTransactionAmount(Double transaction_amount) {
		this.transaction_amount = transaction_amount;
	}
	
	
	
	public Date getTransactionDate() {
		return transaction_date;
	}
	public void setTransactionDate(Date transaction_date) {
		this.transaction_date = transaction_date;
	}
}