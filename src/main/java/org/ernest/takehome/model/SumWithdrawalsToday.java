package org.ernest.takehome.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

/*
 * This is our model class and it corresponds to tb_accounts table in database
 */
@Entity
@Table(name="vw_sumwithdrawalstoday")
public class SumWithdrawalsToday{
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	
	@Column(name="total_withdrawals")
	int total_withdrawals;	

	
	
	public SumWithdrawalsToday() {
		super();
	}
	public SumWithdrawalsToday(int id, int total_withdrawals) {
		super();
		this.id=id;
		this.total_withdrawals = total_withdrawals;
	}
	
	 
	public int getWithdrawalsSum() {
		return total_withdrawals;
	}
	public void setWithdrawalsSum(int total_withdrawals) {
		this.total_withdrawals = total_withdrawals;
	}
	
	
}