package org.ernest.takehome.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/*
 * This is our model class and it corresponds to tb_accounts table in database
 */
@Entity
@Table(name="tbaccounts")
public class Account{
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	
	@Column(name="account_number")
	String account_number;	
	
	public Account() {
		super();
	}
	public Account(int id, String account_number) {
		super();
		this.id = id;
		this.account_number = account_number;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAccountNumber() {
		return account_number;
	}
	public void setAccountNumber(String account_number) {
		this.account_number = account_number;
	}	
	
}