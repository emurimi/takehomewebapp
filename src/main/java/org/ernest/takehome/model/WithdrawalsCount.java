package org.ernest.takehome.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

/*
 * This is our model class and it corresponds to tb_accounts table in database
 */
@Entity
@Table(name="vw_withdrawalscount")
public class WithdrawalsCount{
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	
	@Column(name="withdrawals_count")
	int withdrawals_count;	

	
	
	public WithdrawalsCount() {
		super();
	}
	public WithdrawalsCount(int id, int withdrawals_count) {
		super();
		this.id=id;
		this.withdrawals_count = withdrawals_count;
	}
	
	 
	public int getWithdrawalscount() {
		return withdrawals_count;
	}
	public void setWithdrawalscount(int withdrawals_count) {
		this.withdrawals_count = withdrawals_count;
	}
	
	
}