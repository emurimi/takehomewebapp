package org.ernest.takehome.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

/*
 * This is our model class and it corresponds to tb_accounts table in database
 */
@Entity
@Table(name="vw_sumdepositstoday")
public class SumDepositsToday{
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	
	@Column(name="total_deposits")
	int total_deposits;	

	
	
	public SumDepositsToday() {
		super();
	}
	public SumDepositsToday(int id, int total_deposits) {
		super();
		this.id=id;
		this.total_deposits = total_deposits;
	}
	
	 
	public int getDepositsSum() {
		return total_deposits;
	}
	public void setDepositsSum(int total_deposits) {
		this.total_deposits = total_deposits;
	}
	
	
}