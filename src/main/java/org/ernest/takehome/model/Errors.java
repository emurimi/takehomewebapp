package org.ernest.takehome.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

/*
 * This is our model class and it corresponds to tb_accounts table in database
 */
@Entity
@Table(name="tb_errormessages")
public class Errors{
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	
	@Column(name="code")
	String code;	
	
	@Column(name="message")
	String message;
	
	
	public Errors() {
		super();
	}
	public Errors(int id, String code, String message) {
		super();
		this.id = id;
		this.code = code;
		this.message = message;
	}
	
	 
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
		
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String code) {
		this.code = message;
	}
	
}