package org.ernest.takehome.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

/*
 * This is our model class and it corresponds to tb_accounts table in database
 */
@Entity
@Table(name="vw_depositscount")
public class DepositsCount{
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	
	@Column(name="deposits_count")
	int deposits_count;	

	
	
	public DepositsCount() {
		super();
	}
	public DepositsCount(int id, int deposits_count) {
		super();
		this.id=id;
		this.deposits_count = deposits_count;
	}
	
	 
	public int getDepositscount() {
		return deposits_count;
	}
	public void setDepositscount(int deposits_count) {
		this.deposits_count = deposits_count;
	}
	
	
}