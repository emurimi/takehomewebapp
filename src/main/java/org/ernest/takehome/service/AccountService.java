package org.ernest.takehome.service;

import java.util.List;
import org.ernest.takehome.dao.AccountDAO;
import org.ernest.takehome.model.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("accountService")
public class AccountService {

	@Autowired
	AccountDAO accountDao;
	
	@Transactional
	public List<Account> getAllAccounts() {
		return accountDao.getAllAccounts();
	}
	
	@Transactional
	public Account getCountry(int id) {
		return accountDao.getCountry(id);
	}

	@Transactional
	public void addCountry(Account country) {
		
		accountDao.addCountry(country);
	}

	@Transactional
	public void updateCountry(Account country) {
		accountDao.updateCountry(country);

	}

	@Transactional
	public void deleteCountry(int id) {
		accountDao.deleteCountry(id);
	}
	
	
	
}
