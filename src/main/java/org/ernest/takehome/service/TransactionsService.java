package org.ernest.takehome.service;

import java.util.List;
import org.ernest.takehome.dao.TransactionDAO;
import org.ernest.takehome.model.Account;
import org.ernest.takehome.model.Balance;
import org.ernest.takehome.model.Transaction;
import org.ernest.takehome.model.Errors;
import org.ernest.takehome.model.DepositsCount;
import org.ernest.takehome.model.SumDepositsToday;
import org.ernest.takehome.model.SumWithdrawalsToday;
import org.ernest.takehome.model.WithdrawalsCount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("transactionsService")
public class TransactionsService {

	@Autowired
	TransactionDAO transactionsDao;
	
	@Transactional
	public List<Transaction> getAllTransactions() {
		return transactionsDao.getAllTransactions();
	}
	
//	@Transactional
//	public Account getCountry(int id) {
//		return accountDao.getCountry(id);
//	}
//
	
	@Transactional
	public List<Transaction> addDeposit(Transaction deposit) {
		
		if(deposit.getTransactionAmount()> 40000){
			
			//check if transaction has exceeded per transaction limit
			
			return transactionsDao.getDepositExceedErrorMessage();
		}else{
			
			//check if customer has exceeded daily transactions count limit
			List<DepositsCount> dailyTxnsCount = transactionsDao.checkTrxLimit();
			
			if(dailyTxnsCount.get(0).getDepositscount() >= 4){
				return transactionsDao.getDepositCountExceedErrorMessage();
			}else{
				//check if Maximum amount limit is reached
				List<SumDepositsToday> sumDepositstoday = transactionsDao.checkSumDeposits();				
				if(sumDepositstoday.get(0).getDepositsSum()>= 150000){
					return transactionsDao.getDepositAmountExceedErrorMessage();
				}else{
					
					transactionsDao.addDeposit(deposit);
					return transactionsDao.getErrorMessage();
				}
				
			}
			
		}
		
	}
	
	@Transactional
		public List<Transaction> addWithdraw(Transaction withdraw){
		
		if(withdraw.getTransactionAmount()> 20000){
			
			//check if transaction has exceeded per transaction limit			
			return transactionsDao.getWithdrawExceedErrorMessage();
			
		}else{
			
			//check if customer has exceeded daily transactions count limit
			List<WithdrawalsCount> dailyTxnsCount = transactionsDao.checkTrxLimitWithdrawals();
			
			if(dailyTxnsCount.get(0).getWithdrawalscount() >= 3){
				return transactionsDao.getDepositCountExceedErrorMessage();
			}else{
				//check if Maximum amount limit is reached
				List<SumWithdrawalsToday> sumWithdrawalstoday = transactionsDao.checkSumWithdrawals();				
				if(sumWithdrawalstoday.get(0).getWithdrawalsSum()>= 50000){
					return transactionsDao.getWithdrawalsAmountExceedErrorMessage();
				}else{
					//check if withdrawal amount is more than balance available
					List<Balance> accountbalance = transactionsDao.getAccountBalanceWithdraw();
					Double withdrawalAmount = withdraw.getTransactionAmount();
					if(accountbalance.get(0).getBalance() < withdrawalAmount){
						return transactionsDao.getInsufficientBalanceError();
					}
					transactionsDao.addWithdraw(withdraw);
					return transactionsDao.getSuccessMessageWithdrawal();
				}
				
			}
			
		}

		
	}
	
	@Transactional
	public List<Transaction> getAccountBalance() {
		return transactionsDao.getAccountBalance();
	}
	
	 

	
//
//	@Transactional
//	public void updateCountry(Account country) {
//		accountDao.updateCountry(country);
//
//	}
//
//	@Transactional
//	public void deleteCountry(int id) {
//		accountDao.deleteCountry(id);
//	}
	
	
	
}
