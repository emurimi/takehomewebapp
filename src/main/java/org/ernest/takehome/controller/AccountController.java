package org.ernest.takehome.controller;

import java.util.List;
import org.ernest.takehome.model.Account;
import org.ernest.takehome.model.Transaction;
import org.ernest.takehome.service.AccountService;
import org.ernest.takehome.service.TransactionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountController {
	
	@Autowired
	AccountService accountService;
	
	@Autowired
	TransactionsService transactionsService;
	
	@RequestMapping(value = "/accounts", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<Account> getAccounts() {
		
		List<Account> listOfAccounts = accountService.getAllAccounts();
		return listOfAccounts;
	}
	
	@RequestMapping(value = "/transactions", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<Transaction> getTransactions() {
		
		List<Transaction> listOfTransactions = transactionsService.getAllTransactions();
		return listOfTransactions;
	}
	
	@RequestMapping(value = "/deposit", method = RequestMethod.POST, headers = "Accept=application/json")
	
		public List<Transaction> addDeposit(@RequestBody Transaction deposit) {
				
				List<Transaction> listBalance = transactionsService.addDeposit(deposit);
				return listBalance;
			}
	
	
			@RequestMapping(value = "/withdraw", method = RequestMethod.POST, headers = "Accept=application/json")
			public List<Transaction> addWithdraw(@RequestBody Transaction withdraw) {
				
				List<Transaction> listBalance = transactionsService.addWithdraw(withdraw);
				return listBalance;
			}
				 
	
	
	@RequestMapping(value = "/balance", method = RequestMethod.GET, headers = "Accept=application/json")
	
	public List<Transaction> getAccountBalance() {
		
		List<Transaction> listBalance = transactionsService.getAccountBalance();
		return listBalance;
	}
	
	 

	@RequestMapping(value = "/addCountry", method = RequestMethod.POST, headers = "Accept=application/json")
	public void addCountry(@RequestBody Account country) {	
		accountService.addCountry(country);
		
	}

	@RequestMapping(value = "/updateCountry", method = RequestMethod.PUT, headers = "Accept=application/json")
	public void updateCountry(@RequestBody Account country) {
		accountService.updateCountry(country);
	}

	@RequestMapping(value = "/deleteCountry/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
	public void deleteCountry(@PathVariable("id") int id) {
		accountService.deleteCountry(id);		
	}	
}
